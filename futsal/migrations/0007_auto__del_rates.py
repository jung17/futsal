# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Rates'
        db.delete_table(u'futsal_rates')


    def backwards(self, orm):
        # Adding model 'Rates'
        db.create_table(u'futsal_rates', (
            ('evening', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('morning', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('afternoon', self.gf('django.db.models.fields.CharField')(max_length=20)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'futsal', ['Rates'])


    models = {
        u'futsal.futsal': {
            'Meta': {'object_name': 'Futsal'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'contact_number': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reserved_by': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        }
    }

    complete_apps = ['futsal']