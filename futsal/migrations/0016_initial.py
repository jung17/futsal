# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Futsal'
        db.create_table(u'futsal_futsal', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('reserved_by', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('start_time', self.gf('django.db.models.fields.TimeField')()),
            ('end_time', self.gf('django.db.models.fields.TimeField')()),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('contact_number', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
        ))
        db.send_create_signal(u'futsal', ['Futsal'])

        # Adding model 'Rates'
        db.create_table(u'futsal_rates', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('morning', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('afternoon', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('evening', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'futsal', ['Rates'])


    def backwards(self, orm):
        # Deleting model 'Futsal'
        db.delete_table(u'futsal_futsal')

        # Deleting model 'Rates'
        db.delete_table(u'futsal_rates')


    models = {
        u'futsal.futsal': {
            'Meta': {'object_name': 'Futsal'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'contact_number': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reserved_by': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        },
        u'futsal.rates': {
            'Meta': {'object_name': 'Rates'},
            'afternoon': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'evening': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'morning': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['futsal']