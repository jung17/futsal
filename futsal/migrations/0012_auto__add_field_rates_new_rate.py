# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Rates.new_rate'
        db.add_column(u'futsal_rates', 'new_rate',
                      self.gf('django.db.models.fields.DateField')(default=2008),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Rates.new_rate'
        db.delete_column(u'futsal_rates', 'new_rate')


    models = {
        u'futsal.futsal': {
            'Meta': {'object_name': 'Futsal'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'contact_number': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reserved_by': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        },
        u'futsal.rates': {
            'Meta': {'object_name': 'Rates'},
            'afternoon': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'evening': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'morning': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'new_rate': ('django.db.models.fields.DateField', [], {})
        }
    }

    complete_apps = ['futsal']