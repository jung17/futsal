# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Futsal.start_time'
        db.alter_column(u'futsal_futsal', 'start_time', self.gf('django.db.models.fields.TimeField')())

        # Changing field 'Futsal.end_time'
        db.alter_column(u'futsal_futsal', 'end_time', self.gf('django.db.models.fields.TimeField')())

        # Changing field 'Futsal.date'
        db.alter_column(u'futsal_futsal', 'date', self.gf('django.db.models.fields.DateField')())

    def backwards(self, orm):

        # Changing field 'Futsal.start_time'
        db.alter_column(u'futsal_futsal', 'start_time', self.gf('django.db.models.fields.DateTimeField')())

        # Changing field 'Futsal.end_time'
        db.alter_column(u'futsal_futsal', 'end_time', self.gf('django.db.models.fields.DateTimeField')())

        # Changing field 'Futsal.date'
        db.alter_column(u'futsal_futsal', 'date', self.gf('django.db.models.fields.DateTimeField')())

    models = {
        u'futsal.futsal': {
            'Meta': {'object_name': 'Futsal'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'contact_number': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'end_time': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reserved_by': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'start_time': ('django.db.models.fields.TimeField', [], {})
        }
    }

    complete_apps = ['futsal']