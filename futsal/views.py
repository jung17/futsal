from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render,render_to_response
from futsal.forms import ReserveForm, RatesForm
from django.core.context_processors import csrf
from futsal.models import Futsal
import datetime
from django.utils import timezone
from datetime import timedelta
# Create your views here.
def home(request):
	# c={}
	# c.update(csrf(request))
	date = datetime.datetime.now()
	date_tomorrow = datetime.datetime.now()+datetime.timedelta(days=1)
	obj = Futsal.objects.filter(date = date).order_by('start_time')
	return render(request,'homepage.html',{'obj':obj,'date':date,'date_tomorrow':date_tomorrow})

def league_table(request):
	return render(request,'1.html')

def register(request):
	c={}
	c.update(csrf(request))	
	
	if request.method == 'POST':
		form = ReserveForm(request.POST)
		# form.fields['endtime'].widget.attrs['readonly'] = True

		if form.is_valid():
			
			# time_to_add = request.POST['time_of_play']
			# time = Futsal.objects.all()
			# time.end_time = datetime.datetime.now()
			import pdb
			pdb.set_trace()
			form.save()

			return HttpResponseRedirect('../../futsal/')
	else:
		form = ReserveForm()
		
	c['form'] = form
	# form.fields['end_time'].widget.attrs['readonly'] = True
	return render(request, 'reservation.html', c)

def settings(request):

	if request.method == 'POST':
		form = RatesForm(request.POST)

		if form.is_valid():
			form.save()
			return HttpResponseRedirect('../../futsal/settings/')
	else:
		form = RatesForm()

	c =	{}
	c.update(csrf(request))
	c['form'] = form
	return render(request, 'settings.html', c)

def reservation_detail(request):
	obj = Futsal.objects.all().order_by('date')
	return render(request,'reservation_detail.html',{'obj':obj})

