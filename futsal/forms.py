from django import forms
from futsal.models import Futsal, Rates

class ReserveForm(forms.ModelForm):
	# end_time = form.fields['field'].widget.attrs['readonly'] = True
	
	class Meta:
		model = Futsal
		fields = ['reserved_by', 'start_time', 'end_time', 'time_of_play', 'date', 'contact_number', 'address']


class RatesForm(forms.ModelForm):

	class Meta:
		model = Rates
