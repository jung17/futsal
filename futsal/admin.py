from django.contrib import admin
from futsal.models import Futsal, Rates

class ChoiceInline(admin.TabularInline):
	model = Futsal


class FutsalAdmin(admin.ModelAdmin):
	fields = ['reserved_by', 'start_time', 'end_time', 'date', 'contact_number', 'address', 'time_of_play']

# Register your models here.

admin.site.register(Futsal, FutsalAdmin)
admin.site.register(Rates)
