from django.db import models
import datetime
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.
class Futsal(models.Model):
	play_choices = (
		('1', 'one hour'),
		('2', 'two hour'),
		('3', 'three hour'),
	)
	reserved_by = models.CharField(max_length = 200)
	start_time = models.TimeField('start_time')
	end_time = models.TimeField('end_time')
	time_of_play = models.CharField(max_length=1, choices = play_choices)
	date = models.DateField('date')
	contact_number = models.CharField(max_length=200)
	address = models.CharField(max_length = 200, blank = True)

	def __unicode__(self):
		return self.reserved_by

	# def end_time(self):
	# 	return self.start_time

class Rates(models.Model):
	# new_rate = models.DateField('new_rate')
	rate = models.DateField('rate_updated_date')
	morning = models.CharField(max_length=20)
	afternoon = models.CharField(max_length = 20)
	evening = models.CharField(max_length = 20)

	def __str__(self):
		# return self.new_rate.strftime('%Y-%m-%d')
		return self.rate