from django.conf.urls import patterns, url

from futsal import views

urlpatterns = patterns('futsal',
	url(r'^$', 'views.home', name = 'home'),
	url(r'^register/$', 'views.register', name = 'register'),
	url(r'^league/$', 'views.league_table', name = 'league_table'),
	url(r'^reservation_detail/$','views.reservation_detail', name = 'reservation_detail'),
	url(r'^settings/$','views.settings', name = 'settings'),
	)